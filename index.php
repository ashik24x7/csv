<?php ini_set('max_execution_time', 500);
	$connection = mysqli_connect("localhost","root","","accounts"); 

	function sql_pre($value){
		global $connection;
		$value = str_replace(",", " ", $value);
		return mysqli_real_escape_string($connection,$value);
	}

	function import_csv($data){
		global $connection;
		$name = $data["name"];
        $type = explode(".", $name);
        $type = end($type);

        $allowed = array("csv");
        if(in_array($type,$allowed)){

            $file = fopen("{$name}", "r");
            $num = 1;
            $column = [];
            while ($csv = fgetcsv($file)) {
            	if($num == 1){
            		foreach ($csv as $key => $value) {
            			$column[] = str_replace(" ", "_", strtolower(trim($value)));
            		}
            		$num++;
            	}else{
					if(!empty($csv[13])){
						$tmp_date = explode('/',$csv[13]);
						
						if(empty($tmp_date[0]) || empty($tmp_date[1]) || empty($tmp_date[2])){
							$csv[13] = '0000-00-00 00:00:00';
						}else{
							$date = $tmp_date[2].'-'.$tmp_date[1].'-'.$tmp_date[0];
						
							$csv[13] = date('Y-m-d 00:00:00',strtotime($date));
						}
					}
            		$csv_data = array_values($csv);
            		$csv_column = "`".implode("`,`",$column)."`";
            		$data = "'".implode("','",$csv_data)."'";

            		

            		$query = "INSERT INTO `customer_details` ( ";
            		$query .= $csv_column;
            		$query .= " )VALUES( ";
            		$query .= $data;
            		$query .= ")";


            		$result = mysqli_query($connection,$query);
	            }
            }
            if(mysqli_affected_rows($connection) >= 1){
            	return "CSV file has imported successfully";
            }
        }else{
            return "This is not CSV file";
        }

	}

	if(isset($_REQUEST["submit"])){
        echo import_csv($_FILES["csv"]);
    }


 ?>

 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>Document</title>
 </head>
 <body>
 	<form action="index.php" method="post" enctype="multipart/form-data">
	 	<input type="file" name="csv">
	 	<input type="submit" name="submit" value="Upload">
 	</form>
 </body>
 </html>